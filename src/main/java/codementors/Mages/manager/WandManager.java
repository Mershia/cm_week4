package codementors.Mages.manager;

import codementors.Mages.database.CoreDAO;
import codementors.Mages.database.WandDAO;
import codementors.Mages.database.WoodDAO;
import codementors.Mages.model.Core;
import codementors.Mages.model.Wand;
import codementors.Mages.model.Wood;

import java.sql.Date;
import java.util.Scanner;

/**
 * Created by maryb on 05.07.2017.
 */
public class WandManager extends BaseManager<Wand, WandDAO> {

    public WandManager() {
        dao = new WandDAO();
    }

    @Override
    protected Wand parseNew(Scanner scanner) {

        System.out.println("new production date: ");
        String productionDate = scanner.next();
        Date date = Date.valueOf(productionDate);
        System.out.println("new wood: ");
        int woodId = scanner.nextInt();
        Wood wood = new WoodDAO().find(woodId);
        System.out.println("new core: ");
        int coreId = scanner.nextInt();
        Core core = new CoreDAO().find(coreId);

        return new Wand(wood, core, date);
    }

    @Override
    protected void copyId(Wand from, Wand to) {
        to.setId(to.getId());
    }
}
