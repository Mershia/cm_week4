package codementors.Mages.manager;


import codementors.Mages.database.WoodDAO;
import codementors.Mages.model.Wood;

import java.util.Scanner;


/**
 * Created by maryb on 05.07.2017.
 */
public class WoodManager extends BaseManager<Wood, WoodDAO> {

    public WoodManager(){
        dao = new WoodDAO();
    }

    @Override
    protected Wood parseNew(Scanner scanner) {
        System.out.println("new name wood: ");
        String name = scanner.next();
        System.out.println("new toughness: ");
        int toughness = scanner.nextInt();
        return new Wood(name, toughness);
    }

    @Override
    protected void copyId(Wood from, Wood to) {
        to.setId(to.getId());
    }
}
