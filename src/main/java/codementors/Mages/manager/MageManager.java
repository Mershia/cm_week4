package codementors.Mages.manager;

import codementors.Mages.database.MageDAO;
import codementors.Mages.database.SpellDAO;
import codementors.Mages.database.WandDAO;
import codementors.Mages.model.Mage;
import codementors.Mages.model.Spell;
import codementors.Mages.model.Wand;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Created by maryb on 05.07.2017.
 */
public class MageManager extends BaseManager<Mage, MageDAO> {

    public MageManager() {
        dao = new MageDAO();
    }

    @Override
    protected Mage parseNew(Scanner scanner) {
        System.out.println("new name: ");
        String name = scanner.next();
        System.out.println("new wand id: ");
        int wandId = scanner.nextInt();
        Wand wand = new WandDAO().find(wandId);
        System.out.println("new supervisor id: ");
        int supervisorId = scanner.nextInt();
        Mage mage = new MageDAO().find(supervisorId);
        System.out.println("Do you want to add spell for this mage? (y/n) ");
        String ans = scanner.next();
        List<Spell> spells = new ArrayList<>();
        if (ans.equals("y")) {
            while (true) {
                System.out.println("id of an existing spell ( 0 if stop adding): ");
                int spellId = scanner.nextInt();
                if (spellId == 0) {
                    break;
                }
                spells.add(new SpellDAO().find(spellId));
            }
        }

        return new Mage(name, wand, mage, spells);
    }

    @Override
    protected void copyId(Mage from, Mage to) {
        to.setId(from.getId());
    }

}
