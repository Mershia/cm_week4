package codementors.Mages.manager;

import codementors.Mages.database.SpellDAO;
import codementors.Mages.model.Spell;

import java.util.Scanner;

/**
 * Created by maryb on 05.07.2017.
 */
public class SpellManeger extends BaseManager<Spell, SpellDAO> {

    public SpellManeger(){
        dao = new SpellDAO();
    }

    @Override
    protected Spell parseNew(Scanner scanner) {
        System.out.println("new incantation: ");
        String incantation = scanner.next();

        return new Spell(incantation);

    }

    @Override
    protected void copyId(Spell from, Spell to) {
        to.setId(to.getId());
    }
}

