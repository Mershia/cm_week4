package codementors.Mages.manager;

import codementors.Mages.database.CoreDAO;
import codementors.Mages.model.Core;

import java.util.Scanner;

/**
 * Created by maryb on 05.07.2017.
 */
public class CoreManager extends BaseManager<Core, CoreDAO>{

    public CoreManager() {
        dao = new CoreDAO();
    }

    @Override
    protected Core parseNew(Scanner scanner) {
        System.out.print("new name: ");
        String name = scanner.next();
        System.out.print("new power: ");
        int power = scanner.nextInt();
        System.out.print("new consistency: ");
        int consistency = scanner.nextInt();
        return new Core(name, power, consistency);
    }

    @Override
    protected void copyId(Core from, Core to) {
        to.setId(to.getId());
    }

}
