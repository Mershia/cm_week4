package codementors.Mages.manager;

import codementors.Mages.database.SpellBookDAO;
import codementors.Mages.database.SpellDAO;
import codementors.Mages.model.Spell;
import codementors.Mages.model.SpellBook;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Created by maryb on 05.07.2017.
 */
public class SpellBookManager extends BaseManager<SpellBook, SpellBookDAO> {

    public SpellBookManager(){
        dao = new SpellBookDAO();
    }

    @Override
    protected SpellBook parseNew(Scanner scanner) {
        System.out.println("new title: ");
        String title = scanner.next();
        System.out.println("new author: ");
        String author = scanner.next();
        System.out.println("new publish date: ");
        String publishDate = scanner.next();
        Date date = Date.valueOf(publishDate);
        List<Spell> spells = new ArrayList<>();
        System.out.println("Do you want to add spell for this mage? (y/n) ");
        String ans = scanner.next();
        if (ans.equals("y")) {
            while (true){
                System.out.println("id of an existing spell (0 if stop adding): ");
                int spellId = scanner.nextInt();
                if(spellId == 0) {
                    break;
                }
                spells.add(new SpellDAO().find(spellId));
            }
        }
        return new SpellBook(title, author, date, spells);
    }

    @Override
    protected void copyId(SpellBook from, SpellBook to) {
        to.setId(to.getId());
    }
}
