package codementors.Mages;

import codementors.Mages.manager.CoreManager;
import codementors.Mages.manager.MageManager;
import codementors.Mages.manager.SpellBookManager;
import codementors.Mages.manager.SpellManeger;
import codementors.Mages.manager.WandManager;
import codementors.Mages.manager.WoodManager;

import java.util.Scanner;

/**
 * Created by maryb on 05.07.2017.
 */
public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String command;
        CoreManager coreManager = new CoreManager();
        MageManager mageManager = new MageManager();
        SpellBookManager spellBookManager = new SpellBookManager();
        SpellManeger spellManeger = new SpellManeger();
        WandManager wandManager = new WandManager();
        WoodManager woodManager = new WoodManager();
        boolean run = true;

        while (run) {
            System.out.print("command: ");
            command = scanner.next();

            switch (command) {
                case "core": {
                    coreManager.manage(scanner);
                    break;
                }
                case "mage": {
                    mageManager.manage(scanner);
                    break;
                }
                case "spellbook": {
                    spellBookManager.manage(scanner);
                    break;
                }
                case "spell": {
                    spellManeger.manage(scanner);
                    break;
                }
                case "wand": {
                    wandManager.manage(scanner);
                    break;
                }
                case "wood": {
                    woodManager.manage(scanner);
                    break;
                }

                case "quit": {
                    run = false;
                    break;
                }
            }
        }
    }
}

