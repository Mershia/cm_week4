package codementors.Mages.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by maryb on 05.07.2017.
 */
public abstract class BaseDAO <T> {

    public static final Logger log = Logger.getLogger(BaseDAO.class.getName());

    public abstract String getTableName();

    public abstract T parseValue (ResultSet result) throws SQLException;

    public abstract String [] getColumns();

    public abstract Object [] getColumnsValue (T value);

    public abstract int getPrimaryKeyValue (T value);

    private void setParams (PreparedStatement statement, Object [] values) throws SQLException{

        for (int i = 0; i < values.length; i++){
            Object param = values[i];
            int paramIndex = i + 1;

            if (param instanceof String){
                statement.setString(paramIndex, (String) param);
            }else if (param instanceof Integer){
                statement.setInt(paramIndex, (Integer) param);
            }else{
                throw new IllegalArgumentException("Unsupported class " + param.getClass());
            }
        }
    }

    public void execute (String sqlCommand, Object[] params){
        try (Connection connection = ConnectionFactory.createConnection();
            PreparedStatement statement = connection.prepareStatement(sqlCommand.toString())){
            setParams(statement, params);
            log.log(Level.INFO, statement.toString());
            statement.execute();
        }catch(SQLException ex){
            log.log(Level.SEVERE, ex.getMessage(), ex);
        }
    }

    public List<T> executeQuery (String sglCommand, Object [] params){
        List<T> values = new ArrayList<T>();

        try(Connection connection = ConnectionFactory.createConnection();
        PreparedStatement statement = connection.prepareStatement(sglCommand);){

            setParams(statement, params);
            log.log(Level.INFO, statement.toString());
            try (ResultSet resultSet = statement.executeQuery()){
                while (resultSet.next()){
                    T value = parseValue(resultSet);
                    values.add(value);
                }
            }
        }catch (SQLException ex){
            log.log(Level.SEVERE, ex.getMessage(), ex);
        }
        return values;
    }

    public void update (T value){

        StringBuffer sql = new StringBuffer("UPDATE ");
        sql.append(getTableName()).append(" SET ");

        for (String column : getColumns()){
            sql.append(column).append(" = ?, ");
        }

        sql.replace(sql.length() - 2, sql.length(), " ");
        sql.append("WHERE id = ?");

        Object[] params = getColumnsValue(value);
        params = Arrays.copyOf(params, params.length + 1);
        params[params.length -1] = getPrimaryKeyValue(value);
        execute(sql.toString(), params);
    }

    public void insert (T value){
        StringBuffer sql = new StringBuffer("INSERT INTO ");
        sql.append(getTableName()).append(" (");

        for (String column : getColumns()){
            sql.append(column).append(", ");
        }

        sql.replace(sql.length() -2, sql.length(), ")");
        sql.append(" VALUES (");

        for (int i =0; i <getColumns().length; i++){
            sql.append("?, ");
        }
        sql.replace(sql.length() - 2, sql.length(), ")");
        execute(sql.toString(), getColumnsValue(value));
    }

    public List<T> findAll(){
        String sql = "SELECT * FROM " + getTableName();
        return executeQuery(sql, new Object[0]);
    }

    public void delete(int id){
        String sql = "DELETE FROM " + getTableName() + " WHERE id =?";
        Object[] params = {id};
        execute(sql, params);
    }

    public T find (int id){
        String sql = "SELECT * FROM " + getTableName() + " WHERE id = ?";
        Object[] params = {id};
        return executeQuery(sql, params).get(0);
    }

    public List<Integer> findIntermediateTableIds(String tableName, String columnName, String searchedColumnname, int id ) {
        List<Integer> result = new ArrayList<>();

        String sql = "SELECT " + searchedColumnname + " FROM " + tableName + " WHERE " + columnName + " =?";
        Object[] params = {id};

        try(Connection connection = ConnectionFactory.createConnection();
            PreparedStatement statement = connection.prepareStatement(sql);){

            setParams(statement, params);
            log.log(Level.INFO, statement.toString());
            try (ResultSet resultSet = statement.executeQuery()){
                while (resultSet.next()){
                    result.add(resultSet.getInt(1));
                }
            }
        }catch (SQLException ex){
            log.log(Level.SEVERE, ex.getMessage(), ex);
        }
        return result;
    }

}
