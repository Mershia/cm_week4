package codementors.Mages.database;

import codementors.Mages.model.Spell;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by maryb on 05.07.2017.
 */
public class SpellDAO extends BaseDAO<Spell>{

    private String [] columns = {"incantation"};

    @Override
    public String getTableName() {
        return "spells";
    }

    @Override
    public Spell parseValue(ResultSet result) throws SQLException {
        int id = result.getInt(1);
        String incantation = result.getString(2);

        return new Spell (id, incantation);
    }

    @Override
    public String[] getColumns() {
        return columns;
    }

    @Override
    public Object[] getColumnsValue(Spell value) {
        Object [] values = {value.getIncantation()};

        return values;
    }

    @Override
    public int getPrimaryKeyValue(Spell value) {
        return value.getId();
    }

    @Override
    public void delete(int id) {
        super.delete(id);

        String sql = "DELETE FROM mages_spells WHERE spell =?";
        Object[] params = {id};
        execute(sql, params);

        sql = "DELETE FROM spell_books_spell WHERE spell =?";
        execute(sql, params);
    }
}
