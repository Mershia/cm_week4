package codementors.Mages.database;

import codementors.Mages.model.Spell;
import codementors.Mages.model.SpellBook;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by maryb on 05.07.2017.
 */
public class SpellBookDAO extends BaseDAO<SpellBook>{

    private String [] columns = {"title", "author", "publishYear"};

    @Override
    public String getTableName() {
        return "spell_books";
    }

    @Override
    public SpellBook parseValue(ResultSet result) throws SQLException {

        int id = result.getInt(1);
        String title = result.getString(2);
        String author = result.getString(3);
        Date publishYear = result.getDate(4);
        List<Spell> spells =  new ArrayList<>();

        for (int spellId : findIntermediateTableIds("spell_books_spell", "spell_book", "spell", id)){
            spells.add(new SpellDAO().find(spellId));
        }
        return new SpellBook(id, title, author, publishYear, spells);
    }

    @Override
    public String[] getColumns() {
        return columns;
    }

    @Override
    public Object[] getColumnsValue(SpellBook value) {
        Object [] values = {value.getTitle(), value.getAuthor(), value.getPublishDate()};

        return values;
    }

    @Override
    public int getPrimaryKeyValue(SpellBook value) {
        return value.getId();
    }

    @Override
    public void delete(int id) {
        super.delete(id);
        String sql = "DELETE FROM spell_books_spell WHERE spell_book =?";
        Object[] params = {id};
        execute(sql, params);
    }

    @Override
    public void insert(SpellBook value) {
        super.insert(value);
        String sql = "INSERT INTO spell_books_spell (spell_book, spell) VALUES (?, ?)";

        for(Spell spell : value.getSpells()) {
            Object[] params = {value.getId(), spell.getId()};
            execute(sql, params);
        }

    }

    @Override
    public void update(SpellBook value) {
        super.update(value);

        String sql = "DELETE FROM spell_books_spell WHERE spell_book =?";
        Object[] params1 = {value.getId()};
        execute(sql, params1);

        sql = "INSERT INTO spell_books_spell (spell_book, spell) VALUES (?, ?)";

        for(Spell spell : value.getSpells()) {
            Object[] params2 = {value.getId(), spell.getId()};
            execute(sql, params2);
        }
    }
}
