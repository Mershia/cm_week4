package codementors.Mages.database;

import codementors.Mages.model.Core;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by maryb on 05.07.2017.
 */
public class CoreDAO extends BaseDAO<Core> {
    private String [] colums = {"name", "power", "consistency"};

    @Override
    public String getTableName(){
        return "cores";
    }



    @Override
    public Core parseValue(ResultSet result) throws SQLException {
        int id = result.getInt(1);
        String name = result.getString(2);
        int power = result.getInt(3);
        int consistency = result.getInt(4);

        return new Core (id, name, power, consistency);
    }


    @Override
    public String[] getColumns() {
        return colums;
    }

    @Override
    public Object[] getColumnsValue(Core value) {
        Object[] values = {value.getName(), value.getPower(), value.getConsistency()};

        return values;
    }

    @Override
    public int getPrimaryKeyValue(Core value) {
        return value.getId();
    }
}
