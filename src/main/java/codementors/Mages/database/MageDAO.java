package codementors.Mages.database;

import codementors.Mages.model.Mage;
import codementors.Mages.model.Spell;
import codementors.Mages.model.Wand;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by maryb on 05.07.2017.
 */
public class MageDAO extends BaseDAO<Mage>{

    private String [] columns = {"name", "wand", "supervisor"};

    @Override
    public String getTableName() {
        return "mages";
    }

    @Override
    public Mage parseValue(ResultSet result) throws SQLException {
        int id = result.getInt(1);

        String name = result.getString(2);

        Wand wand = new WandDAO().find(result.getInt(3));

        int supervisorId = result.getInt(4);

        Mage supervisor = supervisorId == 0 ? null : new MageDAO().find(supervisorId);

        List<Spell> spells = new ArrayList<>();

        for(int spellId : findIntermediateTableIds("mages_spells", "mage", "spell", id)) {
            spells.add(new SpellDAO().find(spellId));
        }

        return new Mage(id, name, wand, supervisor, spells);
    }

    @Override
    public String[] getColumns() {
        return columns;
    }

    @Override
    public Object[] getColumnsValue(Mage value) {
        Object [] values = {value.getName(), value.getWand().getId(), value.getSupervisor().getId()};
        return values;
    }

    @Override
    public int getPrimaryKeyValue(Mage value) {
        return value.getId();
    }

    @Override
    public void delete(int id) {
        super.delete(id);
        String sql = "DELETE FROM mages_spells WHERE mage =?";
        Object[] params = {id};
        execute(sql, params);
    }

    @Override
    public void insert(Mage value) {
        super.insert(value);
        String sql = "INSERT INTO mages_spell (mage, spell) VALUES (?, ?)";

        for(Spell spell : value.getSpells()) {
            Object[] params = {value.getId(), spell.getId()};
            execute(sql, params);
        }

    }

    @Override
    public void update(Mage value) {
        super.update(value);

        String sql = "DELETE FROM mages_spells WHERE mage =?";
        Object[] params1 = {value.getId()};
        execute(sql, params1);

        sql = "INSERT INTO mages_spells (mage, spell) VALUES (?, ?)";

        for(Spell spell : value.getSpells()) {
            Object[] params2 = {value.getId(), spell.getId()};
            execute(sql, params2);
        }
    }
}
