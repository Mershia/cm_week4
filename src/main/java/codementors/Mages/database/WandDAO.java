package codementors.Mages.database;

import codementors.Mages.model.Core;
import codementors.Mages.model.Wand;
import codementors.Mages.model.Wood;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by maryb on 05.07.2017.
 */
public class WandDAO extends BaseDAO<Wand>{

    private String [] columns = {"productionDate","wood", "core"};

    @Override
    public String getTableName() {
        return "wands";
    }

    @Override
    public Wand parseValue(ResultSet result) throws SQLException {
        int id = result.getInt(1);
        Date productionDate = result.getDate(2);
        int woodId = result.getInt(3);
        Wood wood = new WoodDAO().find(woodId);
        int coreId = result.getInt(4);
        Core core = new CoreDAO().find(coreId);

        return new Wand(id, productionDate, wood, core);
    }

    @Override
    public String[] getColumns() {
        return columns;
    }

    @Override
    public Object[] getColumnsValue(Wand value) {
        Object[] values = {value.getProductionDate(), value.getWood(), value.getCore()};

        return values;
    }

    @Override
    public int getPrimaryKeyValue(Wand value) {
        return value.getId();
    }
}
