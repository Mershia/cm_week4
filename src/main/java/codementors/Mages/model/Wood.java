package codementors.Mages.model;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * Created by maryb on 05.07.2017.
 */

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@ToString

public class Wood {

    private int id;

    private String name;

    private int toughness;

    public Wood (String name, int toughness){
        this.name = name;
        this.toughness = toughness;
    }
}
