package codementors.Mages.model;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

/**
 * Created by maryb on 05.07.2017.
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@ToString
public class Mage {

    private int id;

    private String name;

    private Wand wand;

    private Mage supervisor;

    private List<Spell> spells;

    public Mage (String name, Wand wand, Mage supervisor, List<Spell> spells){
        this.name = name;
        this.wand = wand;
        this.supervisor = supervisor;
        this.spells = spells;
    }

}
