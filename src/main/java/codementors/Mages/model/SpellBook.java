package codementors.Mages.model;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.sql.Date;
import java.util.List;

/**
 * Created by maryb on 05.07.2017.
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@ToString
public class SpellBook {

    private int id;

    private String title;

    private String author;

    private Date publishDate;

    private List<Spell> spells;

    public SpellBook (String title, String author, Date publishDate, List<Spell> spells){
        this.title = title;
        this.author = author;
        this.publishDate = publishDate;
        this.spells = spells;
    }
}
