package codementors.Mages.model;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.Date;

/**
 * Created by maryb on 05.07.2017.
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@ToString
public class Wand {

    private int id;

    private Date productionDate;

    private Wood wood;

    private Core core;

    public Wand(Wood wood, Core core, Date productionDate) {
        this.productionDate = productionDate;
        this.wood = wood;
        this.core = core;
    }
}

