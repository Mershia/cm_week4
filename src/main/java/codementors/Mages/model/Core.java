package codementors.Mages.model;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * Created by maryb on 05.07.2017.
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@ToString
public class Core {

    private int id;

    private String name;

    private int power;

    private int consistency;

    public Core (String name, int power, int consistency){
        this.name = name;
        this.power = power;
        this.consistency = consistency;
    }


}
